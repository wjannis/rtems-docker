FROM ubuntu:22.04

ARG bsp=rv64imafdc
ARG bsp_noel=noel64imafdc
ARG builder=rtems-riscv
ARG target=riscv
ARG tests=no
ARG smp=yes
ARG version=6

WORKDIR /workspace
#VOLUME /workspace/app
# instead of as a volume lets mount the app folder from the git repo containing already the sample a$
ADD ./app /workspace/app

ENV PREFIX=/workspace/rtems/${version}
ENV BSP=${bsp}
ENV BSP_NOEL=${bsp_noel}
ENV BUILDER=$builder
ENV TARGET=${target}

# first an apt update
RUN echo "===> Update apt package manager..." \
  && apt-get -y update

# ubuntu 22.04 commands to install dependencies
RUN echo " ===> Setting up dependencies..." \
  && apt-get install -y build-essential g++ gdb unzip pax bison flex texinfo python3-dev python-is-python3 libpython2-dev libncurses5-dev zlib1g-dev ninja-build pkg-config git curl

# download rtems source with git (this has the potential to break as it clones the main branch, currently version 6)
RUN echo " ===> Downloading RTEMS sources..." \
  && git clone --depth 1 git://git.rtems.org/rtems-source-builder.git rsb \
  && git clone --depth 1 git://git.rtems.org/rtems.git

# building the cross compiler rtems toolchain
RUN echo " ===> Setting up source builder $BUILDER..." \
  && cd rsb/rtems \
  && ../source-builder/sb-set-builder --prefix=$PREFIX ${version}/$BUILDER

ENV PATH="$PREFIX/bin:$PATH"

RUN echo " ===> Building BSP $BSP for target $TARGET..." \
   && cd rtems \
   && echo [$TARGET/$BSP] > config.ini \
   && echo RTEMS_POSIX_API = True >> config.ini \
   && echo RTEMS_SMP = True >> config.ini \
   && echo [$TARGET/$BSP_NOEL] > config.ini \
   && echo RTEMS_POSIX_API = True >> config.ini \
   && echo RTEMS_SMP = True >> config.ini \
   && cat config.ini \
   && ./waf configure --prefix=/workspace/rtems/6 \
   && ./waf install

RUN echo " ===> Installing QEMU with apt" \
   && apt -y install qemu

CMD /bin/bash
