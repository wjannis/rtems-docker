# RTEMS-Docker

This repo contains a Dockerfile to build a docker images based on Ubuntu 22.04, that builds the RTEMS ecosystems, namely the toolchain (gcc,..), a BSP and mounts a folder with sample apps. It is possible to configure the architecture, specific board, multi processing and more by modifying the variables at the top of the dockerfile.

Current configuration:
- RTEMS version: 6
- Architecture: riscv
- BSP: rv64imadfc and noel64imafdc
- SMP: yes

Tested and also working configuration:
- RTEMS version: 6
- Architecture: Aarch64
- BSP: xilinx-zynqmp-lp64-qemu
- SMP: yes

Tested on Ubuntu 20.04 x86_64.
(Not wokring yet on M1 MacBook Pro)

## Getting started

```bash
ARCH=riscv  # for example
TAG=jannis
docker build -t "${TAG}:RTEMS_${ARCH}"
```

This will probably take a lot of time up to easily an hour, especially building gcc from source. 
So you might want to comment out the lines about building the toolchain and the rtems BSP and run them manually.

## Building an executable

To build an executable, we need the rtems cross compiler toolchain for the specific architecture that is build in the creation of the docker image. If you want to build for a different architecture, check the commands to do so in the Dockerfile. Furthermore we need the board support package (BSP) for our target. This should be also build while the creation of the docker image. There are two sample applications provided with the repo, hello world with and without openmp support. To build the hello executable run:

```bash
cd app/hello
./waf configure --rtems=/workspace/rtems/6 rtems-bsp=$arch/$bsp  # for example riscv/rv64imafdc 
./waf
```

If the commands where succesful, there is now a folder build containing a folder named after architecture and bsp containing the executable.

## Emulating with QEMU

After a succesful build of the executable, it can be run on an emulated QEMU system.

For example for RISC-V 64-Bit:

```bash 
cd /workplace/app/hello/build
qemu-system-riscv64 -nographic -machine virt -m 4064 -smp 4 -net none -serial mon:stdio -bios none -kernel riscv-rtems6-rv64imafdc/hello.exe 
```

Or for an Aarch64 ZynqMP system:

```bash
cd /workplace/app/hello/build
qemu-system-aarch64 -no-reboot -nographic -serial mon:stdio -smp 4 -machine xlnx-zcu102 -m 4096 -kernel aarch64-rtems6-xilinx_zynqmp_lp64_qemu/hello.exe
```

## Port applications to RTEMS
Apart from the very extensive [RTEMS classic API docs](https://docs.rtems.org/branches/master/c-user/index.html) he RTEMS developer created a list of [sample applications](https://docs.rtems.org/branches/master/develenv/sample.html) ([Git Repo here](https://github.com/RTEMS/rtems/tree/master/testsuites/samples)) that should give an idea of how RTEMS applications are working. Similarily, they should function as starting point for own applications.

1. If your application is just C/C++ code, the first thing to do is rewriting the main function, that is the entry point to your program to a rtems task function. __TODO:__ include example, like the [base_sp](https://github.com/RTEMS/rtems/blob/master/testsuites/samples/base_sp/apptask.c) sample.

2. Call the rtems application task you just wrote from the init.c. __TODO:__ explain better what is the rtems [Init()](https://github.com/RTEMS/rtems/blob/master/testsuites/samples/base_sp/init.c) entrypoint anyways..

3. The build system of RTEMS executables is based on [waf](https://waf.io). So compiler and linker flags are defined in the wscript file, i.e. [here]().

4. Try to run the build

## Roadmap
The idea is to save a reproducible snapshot of the preliminary work done by me for the METASAT project. 
Later in the implementation phase of the project we will have an easy setup of everything we need to develop the use cases in RTEMS. 
So the repo should later contain dockerfiles for the architecture (RISC-V), the version 6, smp support and a working QEMU that we want to use, including some demo application that can run.

__UPDATE:__ The docker file including the apps are working now!

## Contributing
Browse or open issues [here](https://gitlab.bsc.es/wjannis/rtems-docker/-/issues).

## Authors and acknowledgment
Fork of [this github repo](https://github.com/bvobart/rtems-docker)

## License
Distributed under MIT license.

